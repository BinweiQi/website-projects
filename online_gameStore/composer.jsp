<html>
  <head>
    <title>Information</title>

    <link rel="stylesheet" type="text/css" href="stylesheet.css">
  </head>
  <body>

    <table>
      <tr>
        <th colspan="2">Composer Information</th>
      </tr>
      <tr>
        <td>Name: </td>
        <td>${requestScope.composer.firstName}</td>
      </tr>
      <tr>
        <td>Company: </td>
        <td>${requestScope.composer.lastName}</td>
      </tr>
      <tr>
        <td>ID: </td>
        <td>${requestScope.composer.id}</td>
      </tr>
      <tr>
        <td>Category: </td>
        <td>${requestScope.composer.category}</td>
      </tr>      
    </table>

    <p>Go back to <a href="index.html" class="link">GameSpeed</a>.Go to <a href="XBox.html" class="link">XBoxs</a>.Go to <a href="PlayStation.html" class="link">PlayStation</a>.Go to <a href="Wii.html" class="link">Wii</a>.Go to <a href="Games.html" class="link">Games</a>.Go to <a href="Warranty.html" class="link">Warranty</a>.</p>

  </body>
</html>

